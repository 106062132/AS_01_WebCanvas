# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
1.Basic control tools:
    按下畫筆跟擦子，即可以使用它們，拖曳他們上方RGB與Size的橫條，可以變更顏色與大小。
2.Text input:
    你可以點擊任何你想要加上字的地方，然後在工具欄中間"Text"旁的格子裡輸入，按下"Text"之後即會出現在你剛剛點擊的地方，左方還有大小跟字型可以選用。
3.Cursor icon:
    按下工具後，游標會變成他們在工具列的樣子。
4.Refresh button:
    按下"Reset"後，版面會重置。
5.Different brush shapes:
    使用圓形、正方形與三角形的刷子，可以畫出不一樣的圖形，同樣的，你可以使用RGB與Size的橫條，變更他們的顏色與大小。
6.Un/Re-do button:
    按下Undo跟Redo之後就會回到上一步或是下一步，但是"Reset"之後會重置。
7.Image tool:
    按下選擇檔案之後，就可以選擇想要上傳的圖片。
8.Download:
    按下"Downland"後，圖片即會下載到電腦裡。
9.背景圖片是自己畫的。
10.謝謝助教跟老師～

