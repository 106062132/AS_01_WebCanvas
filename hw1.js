var canvas = document.getElementById('myCanvas');
var node=1;
var x=0;
var y=0;
var radius=0;
var oricolor="#000000";
var flag_eraser=0;
var flag=0;
document.getElementById("download").addEventListener("click",download,false);
function download(){
    image=canvas.toDataURL("image/png");
    this.href=image;
}

var imageLoader = document.getElementById('file');
imageLoader.addEventListener('change', upload, false);
function upload(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);    
    setTimeout(()=>{cPush()},3);
}

var ctx = canvas.getContext('2d');
ctx.font = "30px Arial";

function tool(x){
    if(flag==0){
        canvas.addEventListener('mousedown', mouseDown);
        canvas.addEventListener('mouseup',mouseUp);
        flag++;
    }
    if(x==5){
        node=5;
        if(flag_eraser==1){
            ctx.strokeStyle=oricolor;
            flag_eraser=0;
        }
        canvas.style.cursor = "url('tris.png'), auto";
    }
    if(x==3){
        node=3;
        if(flag_eraser==1){
            ctx.strokeStyle=oricolor;
            flag_eraser=0;
        }
        canvas.style.cursor = "url('cirs.png'), auto";
    }
    if(x==4){
        node=4;
        if(flag_eraser==1){
            ctx.strokeStyle=oricolor;
            flag_eraser=0;
        }
        canvas.style.cursor = "url('recs.png'), auto";
    }
    if(x==1){
        node=2;
        if(flag_eraser==1){
            ctx.strokeStyle=oricolor;
            flag_eraser=0;
        }
        canvas.style.cursor = "url('brushs.png'), auto";
    }
    if(x==2){
        node=2;
        oricolor=ctx.strokeStyle;
        flag_eraser=1;
        ctx.strokeStyle="#FFFFFF";
        canvas.style.cursor = "url('erasers.png'), auto";
    }
    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }

    
    function mouseUp() {
        canvas.removeEventListener('mousemove', mouseMove);
        cPush();
        console.log('push');
    }
    function mouseDown(evt) {
        if(node==2){
            var mousePos = getMousePos(canvas, evt);
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            evt.preventDefault();
            canvas.addEventListener('mousemove', mouseMove);
        }
        else if(node==3 || node==4 || node==5){
            radius=0;
            var mousePos = getMousePos(canvas, evt);
            x=mousePos.x;
            y=mousePos.y;
            //ctx.beginPath();
            evt.preventDefault();
            canvas.addEventListener('mousemove', mouseMove);
        }
    }
    function mouseMove(evt) {
        if(node==2){
            var mousePos = getMousePos(canvas, evt);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
        }
        else if(node==3){
            radius+=1;
            canvasPic.src = cPushArray[cStep];
            var mousePos = getMousePos(canvas, evt);
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
            }
            ctx.beginPath();
            ctx.arc(x, y,Math.pow(Math.pow(mousePos.x-x,2)+Math.pow(mousePos.y-y,2),1/2)  , 0, 2 * Math.PI);
            setTimeout(()=>{ctx.stroke()},3);
            ctx.closePath();
        }
        else if(node==4){
            radius+=1;
            canvasPic.src = cPushArray[cStep];
            var mousePos = getMousePos(canvas, evt);
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
            }
            ctx.beginPath();
            ctx.rect(x, y, mousePos.x-x, mousePos.y-y);
            setTimeout(()=>{ctx.stroke()},3);
            ctx.closePath();
        }
        else if(node==5){
            radius+=1;
            canvasPic.src = cPushArray[cStep];
            var mousePos = getMousePos(canvas, evt);
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
            }
            ctx.beginPath();
            ctx.moveTo(x, y);
            ctx.lineTo(x-mousePos.x+x, y+mousePos.y-y);
            ctx.lineTo(x+mousePos.x-x, y+mousePos.y-y);
            ctx.lineTo(x, y);
            setTimeout(()=>{ctx.stroke()},3);
            ctx.closePath();
        }
    }
}

function ini(){
    ctx.fillStyle="white";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    canvasPic = new Image();
    cPushArray = new Array();
    cStep = -1;
    cPush();
}

function processcolor(c1,c2,c3) {
    if(flag_eraser==0){
        var cr1=document.getElementById(c1).value;
        var cr2=document.getElementById(c2).value;
        var cr3=document.getElementById(c3).value;
        ctx.strokeStyle="rgb("+cr1+","+cr2+","+cr3+")" 
    }
}

function processsize(s1) {
    ctx.lineWidth=s1;
}


function processtsize(s1,f1) {
    var ss1=document.getElementById(s1).value;
    var ff1=document.getElementById(f1).value;
    ctx.font = ss1+"px "+ff1;
}

function processtext(t1){
    ctx.fillStyle="black";
    // ctx.fillText(document.getElementById(t1).value, 200, 150);
    ctx.fillText(document.getElementById(t1).value, mouseX, mouseY);
    console.log(1);
    cPush();
    ctx.fillStyle="white";
}

function reset(){
    ini();
}
var canvasPic = new Image();
var cPushArray = new Array();
var cStep = -1;
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('myCanvas').toDataURL());
    document.title = cStep + ":" + cPushArray.length;
}
async function cUndo() {
    if (cStep > 0) {
        cStep--;
        // var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic, 0, 0); 
        }
        document.title = cStep + ":" + cPushArray.length;
    }
   // console.log(cPushArray);
}
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        // var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic, 0, 0); 
        }
        document.title = cStep + ":" + cPushArray.length;
    }
}

var mouseX = 0;
var mouseY = 0;
var startingX = 0;

canvas.addEventListener("click",function(e){
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    startingX = mouseX;
    return false;
},false);

/*document.addEventListener("keydown",function(e){
    ctx.fillText(e.key,mouseX,mouseY);
    mouseX += ctx.measureText(e.key).width;
})*/
